/****************************  HEX Package  ************************************
**                                                                            **
**                   Copyright 2011 by Movimento Inc.                         **
**                     http://www.movimento-inc.com       			              **
**                                                                            **
** This software is furnished under a NDA and may not be                      **
** distributed except within the company to which it was provided.         	  **
**                                                                            **
********************************************************************************
**
** This package contains functions useful when communicating over GM-LAN
*/

/******  SUPPORT INFORMATION  *******
**	Email:	support@movimento-inc.com
************************************/

/******  USING PACKAGES  ******/
/******************************/

/******  VERSION INFORMATION ******/
const string GM_LAN_VERSION	= "v2.3";
/*                             A  Ensure that these
**                             |  values   |
** Revision Log:               |  match    |                           col.80->|
** --------------------------------------- V -----------------------------------
** 2015/09/16			DEL   v2.3
** - Added canClearReceiveBuffer() to ClearDTCsinPrimaryCan() because of buffer overflows
**
** 2013/01/16			DRW		v2.2
** - Added if (DebugOn) to print in ClearDTCsinPrimaryCan()
**
** 2012/06/08			Meyyappan Meyyappan		v2.1
** - Fixed the issue on the Clear DTC, to wait for the complete period
**
** 2012/01/04			Meyyappan Meyyappan		v2.0
** - Added Clear DTC's function
**
**	2012/01/04			D.Wybo						 v1.3
** - fixed the while termination condition in DiscoverCANIDs, 
** - should be while(stat) not while (!stat) opposite of DiscoverCANID
**
**	2012/01/03			D.Wybo						 v1.2
** - Added DiscoverCANIDs to report all modules on the specified bus
**
**	2011/06/23			D.Wybo						 v1.1
** - Conditional printing of debug msgs
**
**
**	2011/06/23			Z.A.Klajda						 v1.0
** - Initial version of this package
**
**  -- End Header --
*******************************************************************************/

// GMLAN Functional Messages (GFM)
// GMLAN messages are padded with 0x00, if message is less than 8 data bytes
const CanFrame GFM_HVWU               = {0x100,false,[0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00]};
const CanFrame GFM_SWC_WAKEUP         = {0x101,false,[0xFD,0x02,0x10,0x04,0x00,0x00,0x00,0x00]};
const CanFrame GFM_REQ_B0             = {0x101,false,[0xFE,0x02,0x1A,0xB0,0x00,0x00,0x00,0x00]}; 
const CanFrame GFM_INIT_DIAG          = {0x101,false,[0xFE,0x02,0x10,0x02,0x00,0x00,0x00,0x00]}; 
const CanFrame GFM_STOP_NORM_COMM     = {0x101,false,[0xFE,0x01,0x28,0x00,0x00,0x00,0x00,0x00]};
const CanFrame GFM_REQ_PGM_STATE      = {0x101,false,[0xFE,0x01,0xA2,0x00,0x00,0x00,0x00,0x00]};
const CanFrame GFM_READ_DTCS_EPS      = {0x242,false,[0x03,0xA9,0x81,0x12,0x00,0x00,0x00,0x00]};   // Read All DTCs EPS

const CanFrame GFM_DISABLE_NORMAL            = {0x101,false,[0xFE,0x01,0x28,0x00,0x00,0x00,0x00,0x00]};   // Disable Normal Communications
const CanFrame CHK_NSPGMGMODE_AVAIL          = {0x101,false,[0xFE,0x02,0xA5,0x01,0x00,0x00,0x00,0x00]};   // Normal Speed Programming Mode
const CanFrame CHK_HSPGMGMODE_AVAIL          = {0x101,false,[0xFE,0x02,0xA5,0x02]};   // High Speed Programming Mode
const CanFrame ENBL_PGMG_MODE                = {0x101,false,[0xFE,0x02,0xA5,0x03,0x00,0x00,0x00,0x00]};   // Enable Programming Mode

const CanFrame GFM_TESTER_PRESENT            = {0x101,false,[0xFE,0x01,0x3E,0x00,0x00,0x00,0x00,0x00]};   // Tester Present

const CanFrame GFM_NORM                      = {0x101,false,[0xFE,0x01,0x20,0x00,0x00,0x00,0x00,0x00]};   // Return to Normal Communications
const CanFrame GFM_CLEAR_DTCS                = {0x101,false,[0xFE,0x01,0x04,0x00,0x00,0x00,0x00,0x00]};   // Clear DTCs command
const CanFrame GFM_READ_DTCS                 = {0x101,false,[0xFE,0x03,0xA9,0x81,0xFF,0x00,0x00,0x00]};   // Read All DTCs

const CanFrame RELEARN_VIN                   = {0,false,[0x03,0x3B,0x12,0x08,0x00,0x00,0x00,0x00]};

const string  GM_BOOTLOADER_VALUE = ""; // "2096302";


package GMLAN203
{
  // <local type defs>
  
  // <local functions>
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Func     : GML_LogCANMsg()
  // Notes    : 
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  GML_LogCANMsg(const CanFrame& lpj_CanMsg)
  {
	  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	  // if(!LOG_CAN_MSGS)	return;
	
	  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    short             ls_Tmp;
    string 	          lt_Tmp;
    unsigned char[8]  lauc_Tmp;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if(lpj_CanMsg.data.size() < 8)
    {
      lauc_Tmp[0] = lauc_Tmp[1] = lauc_Tmp[2] = lauc_Tmp[3] = 
      lauc_Tmp[4] = lauc_Tmp[5] = lauc_Tmp[6] = lauc_Tmp[7] = 0x00;
    }
    for(ls_Tmp = 0;ls_Tmp < lpj_CanMsg.data.size();ls_Tmp++)
    {
      lauc_Tmp[ls_Tmp] = lpj_CanMsg.data[ls_Tmp];
    }
  
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    sprintf(lt_Tmp,"%.4X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X",
    lpj_CanMsg.id,lauc_Tmp[0],lauc_Tmp[1],lauc_Tmp[2],lauc_Tmp[3],
    lauc_Tmp[4],lauc_Tmp[5],lauc_Tmp[6],lauc_Tmp[7]);
    print(lt_Tmp);
  }
  
 //Discover CAN ID
 
 	bool DiscoverCANID(CanHandle hCAN, unsigned char TargetAddr, unsigned long TimeOut, unsigned long &moduleID, unsigned long &testerID, bool DebugOn)
	{
    bool stat = false;
    bool TimeUp;
    unsigned int64 stTime = getTime();
    unsigned int64 tStamp;
    CanFrame rxFrame;
    buffer rxData;
    unsigned char svcID;
    unsigned int64 tOut = TimeOut * 1000;
	string		lt_Msg;
    
    do
    {
      if(!stat)
      {
        canSendFrame(hCAN, GFM_REQ_B0);
      }
      canReceiveFrame(hCAN, rxFrame, tStamp, stat, 100);
	  
	  if(rxFrame.data.size() > 3)
	  {
		sprintf(lt_Msg,"DiscoverCANID().Target Addr:0x%X, Found Addr:0x%X",TargetAddr,rxFrame.data[3]);
		print(lt_Msg);
	  }
		
      wait(20);
      
      if(stat && rxFrame.data.size() > 3 && rxFrame.data[3] == TargetAddr)
      {
        moduleID = ((rxFrame.id & 0xFF) | 0x200); // gives us a Physical Request CAN ID
        testerID = ((rxFrame.id & 0xFF) | 0x600); // Module responds to the global request on its USDT Response ID
      }
      else // Not what we are looking for
      {
        moduleID = 0;
        testerID = 0;
        stat = false;
      }
      
      TimeUp = (tStamp > (stTime + tOut));
    } while(!stat && !TimeUp);
    
    return stat;
	}
  
 	bool DiscoverCANIDs(CanHandle hCAN, unsigned long TimeOut, unsigned long[] &moduleID, unsigned long[] &testerID, bool DebugOn)
	{
    bool stat = false;
    bool TimeUp;
    unsigned int64 stTime = getTime();
    unsigned int64 tStamp;
    CanFrame rxFrame;
    buffer rxData;
    unsigned char svcID;
    unsigned int64 tOut = TimeOut * 1000;
    unsigned char offset = 0;
    
    canSendFrame(hCAN, GFM_REQ_B0);
    //wait(20);
    
    do
    {
      canReceiveFrame(hCAN, rxFrame, tStamp, stat, 50);
      if (DebugOn)
      {
        print(rxFrame.getStringValue());
      }
      //wait(50);
      
      if(stat && (rxFrame.data.size() > 3) && (rxFrame.data[1] == 0x5a) && (rxFrame.data[2] == 0xB0) )
      {
        moduleID[offset] = ((rxFrame.id & 0xFF) | 0x200); // gives us a Physical Request CAN ID
        testerID[offset++] = ((rxFrame.id & 0xFF) | 0x600); // Module responds to the global request on its USDT Response ID
      }
      else // Not what we are looking for
      {
        //continue;
      }
      
      TimeUp = (tStamp > (stTime + tOut));
    } while(stat && !TimeUp);
    
    return stat;
	}
  
  bool SendAndReceiveInPrimaryCAN(CanHandle hCAN, const CanFrame& oFrameData, unsigned long TimeOut, buffer& bufRespData, bool DebugOn)
	{
    bool stat;
    unsigned int64 tStamp;
    CanFrame rxFrame;
    buffer rxData;
    unsigned char svcID;
    
    long tOut = TimeOut * 1000;
    
    if(TimeOut <= 100)
    {
      tOut = 100;
    }
    else
    {
      tOut = TimeOut;
    }
    
//    if(DebugOn)
//      print("SendAndReceiveInPrimaryCAN >> Sending Data " + oFrameData.getStringValue());
   
    canSendFrame(hCAN, oFrameData);
    if(DebugOn) GML_LogCANMsg(oFrameData);
    
    canReceiveFrame(hCAN, rxFrame, tStamp, stat, tOut);
    if(DebugOn) GML_LogCANMsg(rxFrame);
    
    //print("SendAndReceiveInPrimaryCAN >> Receving Data " + rxFrame.getStringValue());
    
    if(rxFrame.data.size() < 2)
    {
      return false;
    }
    bufRespData = rxFrame.data;
    
    if(DebugOn)
      printf("SendAndReceiveInPrimaryCAN >> RetCode %s, Status %s" ,stat.getStringValue() , (((oFrameData.data[2] + 0x40) == rxFrame.data[1])? "success":"failure"));
    
    wait(20);
      
    return stat;
	}
  
  //CURRENTLY Works for EPS Module, needs to be changed later.
  bool ReadDTCsInPrimaryCAN(CanHandle hCAN, unsigned long TimeOut,string[] &dtcs, bool DebugOn)
	{
    bool stat = false;
    bool bResult = false;
    bool TimeUp;
    unsigned int64 stTime = getTime();
    unsigned int64 tStamp;
    CanFrame rxFrame;
    unsigned int64 tOut = TimeOut * 1000;
    short nDTCCount =0 ;
    string strTempDTC;
    buffer rxData;
    unsigned long nTemp;
    
    
    canSendFrame(hCAN, GFM_READ_DTCS_EPS);
    if(DebugOn) GML_LogCANMsg(GFM_READ_DTCS_EPS);
    // rxData = [0x81,0x45,0x6D,0x12,0x00];
    do
    {
      canReceiveFrame(hCAN, rxFrame, tStamp, stat, 50);
      
      if (DebugOn)
      {
        print(rxFrame.getStringValue());
      }
 
      rxData = rxFrame.data;
      
      if(stat && (rxData.size() > 3) && (rxData[0] == 0x81))
      {
        if(rxData[1] == 0x00 && rxData[2] == 0x00 && rxData[3] == 0x00)
        {
          stat = false;
          bResult = true; //Got the end of DTC message
        }
        else
        {
          nTemp = (rxData[1] & 0x0F);
    
          if((rxData[1] >> 4) == 4)
          {
            strTempDTC = "C0";
          }
          if((rxData[1] >> 4) == 0xC)
          {
            strTempDTC = "U0";
          }
          
          strTempDTC += nTemp.getStringValue();
          
          sprintf(strTempDTC,"%s%02X",strTempDTC,rxData[2]);
          if(DebugOn)
          {
            printf("DTC %d - %s",nDTCCount, strTempDTC);
          }
          
          dtcs[nDTCCount++] = strTempDTC;
          strTempDTC = "";
          /*
           if(rxData[1] == 0x45)
          {
            rxData = [0x81,0x00,0x00,0x00,0x11];
          }
          */

        }
      
      }
      else // Not what we are looking for
      {
        //continue;
      }
      
      TimeUp = (tStamp > (stTime + tOut));
    } while(stat && !TimeUp);
    
    if(dtcs.size() > 0)
      return true;
      
    return bResult;
	}
   bool ClearDTCsInPrimaryCAN(CanHandle hCAN, unsigned long TimeOut,string[] &ModuleIds, bool DebugOn)
  {
    bool            lb_GotResponse;
    unsigned long   lun64_us_TimeStamp = 0;
    unsigned int64  stTime = getTime();
    CanFrame        lauc_RxFrame;
    unsigned long   lul_TimeOut   = 1000;
    short           ls_Ctr = 0;
    string          lt_Tmp;
    short           nTempCount = 0;
    
    canSetFilter(hCAN,0x600,0xF000,false);
    
    if(TimeOut > lul_TimeOut)
      lul_TimeOut = TimeOut;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    canSendFrame(hCAN,GFM_CLEAR_DTCS);
    if(DebugOn) GML_LogCANMsg(GFM_CLEAR_DTCS);
    string strTemp;
    unsigned int64 EndTime;
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    do
    {
      lb_GotResponse = false;
      canReceiveFrame(hCAN,lauc_RxFrame,lun64_us_TimeStamp,lb_GotResponse,lul_TimeOut);
      
      if(lb_GotResponse && lauc_RxFrame.data.size() > 1)
      {
        if(DebugOn) print(lauc_RxFrame.data.getStringValue());
        if(lauc_RxFrame.data[1] == 0x44) 
        {
          sprintf(strTemp,"%X - PositiveResp",((lauc_RxFrame.id & 0xFF) | 0x200));
          ModuleIds[nTempCount++] = strTemp;
          if(DebugOn) print(strTemp);
        }
        /* //We don't want this now...
        else
        {
          sprintf(strTemp,"%X - No Positive Resp (0x%X)",((lauc_RxFrame.id & 0xFF) | 0x200),lauc_RxFrame.data[1]);
          ModuleIds[nTempCount++] = strTemp;
        }*/
      }
        
      lauc_RxFrame.data.clear();
      canClearReceiveBuffer(hCAN); //Clear buffers
      EndTime = getTime();
      
    } while(EndTime <= stTime + 3500000);
    
    return true;
  }
}
