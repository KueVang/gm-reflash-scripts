// ====================================================================
// <internal package names>
// ====================================================================


// ====================================================================
// <global constants>
// ====================================================================



// ====================================================================
// <package_name>
// ====================================================================
package C2W0004
{
  // ==================================================================
  // <local type defs>
  // ==================================================================
  typedef struct
  {
    string          mt_ModuleName;
    string          mt_Step;      // Vehicle flash sequence step, never modified here!
    string          mt_Operation; // Operation type
    string          mt_EndMode;   // End with reset() or stop(), R or S
    string[]        mat_Parm;      // Parms
    string          mt_Result;    // Final results, P=pass,F=fail,E=error,R=rerun,N=not applic
    string          mt_OutMsg;    // Message if Error or Fail
  } RunInfoType;

  
  // ==================================================================
  // <local invocables>
  // ==================================================================
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // C2W_ReadRunInfo()
  // Example input string = 03,S,FLASHECU,SD/DATA/TUNER/V217_UQA,
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Changes for C2W004: Lines 92 - 94, indices changed to comply with C2W format change in GMFR108D and GMFLS41D
  short C2W_ReadRunInfo(RunInfoType& lpj_RunInfo, short nLine)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    FileHandle  lh_File = 0;
    string      lt_RawString;
    string[]    lat_SubStrings;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if((lh_File = fileOpen("/sd/MSGS/C2W.txt",Read)) == 0)
      return C2W_PostErrorMsg("C2W_ReadRunInfo() C2W.txt file open error");
      
      //Read next line
      short nCurLine = 0;
      do{
          if(!fileGetTextLineAsString(lh_File,lt_RawString))
          {
            if(nLine > 0)
            {
              return 0; //End of file
            }
            else
            {
              return C2W_PostErrorMsg("C2W_ReadRunInfo() C2W.txt file read error");
            }
          }
          nCurLine++;
            
      }while(nCurLine <= nLine);
    
    fileClose(lh_File); lh_File = 0; wait(3000);
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if(lt_RawString.empty())
      return C2W_PostErrorMsg("C2W_ReadRunInfo() C2W.txt file empty error");
      
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    lpj_RunInfo.mt_Step.clear();
    lpj_RunInfo.mat_Parm.clear();
    lpj_RunInfo.mt_OutMsg.clear();
    lpj_RunInfo.mt_Result = "F";     // default value, changes later.
      
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    lat_SubStrings = lt_RawString.split(',');
    if(lat_SubStrings.size() < 4) 
      return C2W_PostErrorMsg("C2W_ReadRunInfo() C2W.txt incorrect data fields error");
    
    lpj_RunInfo.mt_Step        = lat_SubStrings[0];
    lpj_RunInfo.mt_EndMode     = lat_SubStrings[1]; // bump by one this index and the two used below
    lpj_RunInfo.mt_Operation   = lat_SubStrings[2];
    
    // lat_SubStrings[3] is the NodeID of the module specified by this line in the C2W
    // The Node ID is only relevant in the feeder to sort the C2W to ensure BCM is last in the flash list
    for(short ls_Tmp = 4;ls_Tmp < lat_SubStrings.size();ls_Tmp++)
      lpj_RunInfo.mat_Parm[ls_Tmp - 4] = lat_SubStrings[ls_Tmp];
    
    return 1;
  }
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // C2W_WriteRunInfo()
  // Example output string = 03,FLASHECU,P,Some message
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  bool W2C_WriteRunInfo(RunInfoType& lpj_RunInfo)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    FileHandle  lh_File = 0;
    string      lt_Tmp;
    string      lt_Out;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if((lh_File = fileOpen("/sd/MSGS/W2C.txt",Write)) == 0)
      return C2W_PostErrorMsg("C2W_WriteRunInfo() W2C.txt file open error");
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if(lpj_RunInfo.mt_Result == "P")
    {
      if(lpj_RunInfo.mt_OutMsg.empty())
      lpj_RunInfo.mt_OutMsg = "Program completed successfully";
    }
    else
    {
      if(lpj_RunInfo.mt_OutMsg.empty())
      lpj_RunInfo.mt_OutMsg = "Fail or Error cause not specified";
    }
    
    sprintf(lt_Out,"%s,%s,%s,%s",lpj_RunInfo.mt_Step,lpj_RunInfo.mt_Operation,
          lpj_RunInfo.mt_Result,lpj_RunInfo.mt_OutMsg);
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    if(!filePutTextLineAsString(lh_File,lt_Out)) 
      return C2W_PostErrorMsg("C2W_WriteRunInfo() W2C.txt file write error");
    
    fileClose(lh_File); lh_File = 0; wait(5000);
    
    print(lt_Out);
    
    return true;
  }
  
    
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  unsigned char C2W_TwoCharHexStringToUChar(string lt_HexString)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    unsigned char luc_Tmp = 0x00;
    unsigned char luc_Ans = 0x00;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    if(lt_HexString.size() < 2) return 0;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    for(short ls_Tmp = 0;ls_Tmp < 2;ls_Tmp++)
    {
      switch(lt_HexString[ls_Tmp])
      {
        case '0': luc_Tmp = 0x00; break;
        case '1': luc_Tmp = 0x01; break;
        case '2': luc_Tmp = 0x02; break;
        case '3': luc_Tmp = 0x03; break;
        case '4': luc_Tmp = 0x04; break;
        case '5': luc_Tmp = 0x05; break;
        case '6': luc_Tmp = 0x06; break;
        case '7': luc_Tmp = 0x07; break;
        case '8': luc_Tmp = 0x08; break;
        case '9': luc_Tmp = 0x09; break;
        case 'A': luc_Tmp = 0x0A; break;
        case 'B': luc_Tmp = 0x0B; break;
        case 'C': luc_Tmp = 0x0C; break;
        case 'D': luc_Tmp = 0x0D; break;
        case 'E': luc_Tmp = 0x0E; break;
        case 'F': luc_Tmp = 0x0F; break;
        case 'a': luc_Tmp = 0x0A; break;
        case 'b': luc_Tmp = 0x0B; break;
        case 'c': luc_Tmp = 0x0C; break;
        case 'd': luc_Tmp = 0x0D; break;
        case 'e': luc_Tmp = 0x0E; break;
        case 'f': luc_Tmp = 0x0F; break;
      }
      
      if(ls_Tmp == 0) luc_Ans = luc_Tmp * 0x10;
      else luc_Ans += luc_Tmp;
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    return luc_Ans;
  }  
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  unsigned long C2W_ThreeCharHexStringToULong(string lt_HexString)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    if(lt_HexString.size() < 3) return 0;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    unsigned char luc_MSB = (unsigned char) lt_HexString[0]; 
    unsigned char luc_2SB = (unsigned char) lt_HexString[1];
    unsigned char luc_LSB = (unsigned char) lt_HexString[2];
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    if(luc_MSB >= 0x30 && luc_MSB <= 0x39) luc_MSB -= 0x30;
    else if(luc_MSB >= 0x41 && luc_MSB <= 0x46) luc_MSB -= 0x31;
    
    if(luc_2SB >= 0x30 && luc_2SB <= 0x39) luc_2SB -= 0x30;
    else if(luc_2SB >= 0x41 && luc_2SB <= 0x46) luc_2SB -= 0x31;
    
    if(luc_LSB >= 0x30 && luc_LSB <= 0x39) luc_LSB -= 0x30;
    else if(luc_LSB >= 0x41 &&luc_LSB <= 0x46) luc_LSB -= 0x31;
    
    return ((luc_MSB * 0x100) + (luc_2SB * 0x10) + luc_LSB);
  }  
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  string C2W_ULongToCharHexString(const unsigned long lul_DecVel)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    unsigned long   lul_Tmp       = lul_DecVel;
    unsigned long[] laul_DecVal;            
    short           ls_Tmp        = 0;
    short           ls_Cnt        = 0;
    string          lt_HexStr;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    while(lul_Tmp > 0)
    {
      laul_DecVal[ls_Tmp++] = lul_Tmp % 16;
      lul_Tmp /= 16;
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    while(--ls_Tmp >= 0)
    {
      switch(laul_DecVal[ls_Tmp])
      {
        case  0 : lt_HexStr += "0";  break;
        case  1 : lt_HexStr += "1";  break;
        case  2 : lt_HexStr += "2";  break;
        case  3 : lt_HexStr += "3";  break;
        case  4 : lt_HexStr += "4";  break;
        case  5 : lt_HexStr += "5";  break;
        case  6 : lt_HexStr += "6";  break;
        case  7 : lt_HexStr += "7";  break;
        case  8 : lt_HexStr += "8";  break;
        case  9 : lt_HexStr += "9";  break;
        case 10 : lt_HexStr += "A";  break;
        case 11 : lt_HexStr += "B";  break;
        case 12 : lt_HexStr += "C";  break;
        case 13 : lt_HexStr += "D";  break;
        case 14 : lt_HexStr += "E";  break;
        case 15 : lt_HexStr += "F";  break;
      }
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    return lt_HexStr;
  }
  
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  unsigned long C2W_HexStringToULong(const string lt_HexString)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    unsigned long   lul_Tmp       = 0;
    unsigned long   lul_Val       = 0;
    short           ls_Tmp        = 0;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    for(short ls_Tmp = 0;ls_Tmp < lt_HexString.size();ls_Tmp++)
    {
      switch(lt_HexString[ls_Tmp])
      {
        case '0' : lul_Tmp =  0;  break;
        case '1' : lul_Tmp =  1;  break;
        case '2' : lul_Tmp =  2;  break;
        case '3' : lul_Tmp =  3;  break;
        case '4' : lul_Tmp =  4;  break;
        case '5' : lul_Tmp =  5;  break;
        case '6' : lul_Tmp =  6;  break;
        case '7' : lul_Tmp =  7;  break;
        case '8' : lul_Tmp =  8;  break;
        case '9' : lul_Tmp =  9;  break;
        case 'A' : lul_Tmp = 10;  break;
        case 'B' : lul_Tmp = 11;  break;
        case 'C' : lul_Tmp = 12;  break;
        case 'D' : lul_Tmp = 13;  break;
        case 'E' : lul_Tmp = 14;  break;
        case 'F' : lul_Tmp = 15;  break;
        case 'a' : lul_Tmp = 10;  break;
        case 'b' : lul_Tmp = 11;  break;
        case 'c' : lul_Tmp = 12;  break;
        case 'd' : lul_Tmp = 13;  break;
        case 'e' : lul_Tmp = 14;  break;
        case 'f' : lul_Tmp = 15;  break;
      }
      
      lul_Val *= 0x10;
      lul_Val += lul_Tmp;
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    return lul_Val;
  }  
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  unsigned long C2W_DecimalStringToULong(string lt_DecString)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    unsigned long   lul_Val = 0;
    unsigned long   lul_Tmp = 0;
    short           ls_Len  = (short) lt_DecString.size();
    double          ld_Base = 10.0;
    double          ld_Exp  = 0.0;
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~     
    for(short ls_Tmp = ls_Len - 1;ls_Tmp >= 0;ls_Tmp--)
    {
      lul_Tmp  = (unsigned long) lt_DecString[ls_Tmp]; 
      lul_Tmp -= 0x30;
      ld_Exp   = (double) (ls_Len - ls_Tmp - 1);
      lul_Tmp *= (unsigned long) pow(ld_Base,ld_Exp);
      lul_Val += lul_Tmp;
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    
    return lul_Val;
  }  
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  char C2W_HexValueToAlphaNumericASCIIChar(const unsigned char luc_HexVal)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    switch(luc_HexVal)
    {
      case 0x30 : return '0'; break;
      case 0x31 : return '1'; break;
      case 0x32 : return '2'; break;
      case 0x33 : return '3'; break;
      case 0x34 : return '4'; break;
      case 0x35 : return '5'; break;
      case 0x36 : return '6'; break;
      case 0x37 : return '7'; break;
      case 0x38 : return '8'; break;
      case 0x39 : return '9'; break;
      case 0x41 : case 0x61 : return 'A'; break;
      case 0x42 : case 0x62 : return 'B'; break;
      case 0x43 : case 0x63 : return 'C'; break;
      case 0x44 : case 0x64 : return 'D'; break;
      case 0x45 : case 0x65 : return 'E'; break;
      case 0x46 : case 0x66 : return 'F'; break;
      case 0x47 : case 0x67 : return 'G'; break;
      case 0x48 : case 0x68 : return 'H'; break;
      case 0x49 : case 0x69 : return 'I'; break;
      case 0x4A : case 0x6A : return 'J'; break;
      case 0x4B : case 0x6B : return 'K'; break;
      case 0x4C : case 0x6C : return 'L'; break;
      case 0x4D : case 0x6D : return 'M'; break;
      case 0x4E : case 0x6E : return 'N'; break;
      case 0x4F : case 0x6F : return 'O'; break;
      case 0x50 : case 0x70 : return 'P'; break;
      case 0x51 : case 0x71 : return 'Q'; break;
      case 0x52 : case 0x72 : return 'R'; break;
      case 0x53 : case 0x73 : return 'S'; break;
      case 0x54 : case 0x74 : return 'T'; break;
      case 0x55 : case 0x75 : return 'U'; break;
      case 0x56 : case 0x76 : return 'V'; break;
      case 0x57 : case 0x77 : return 'W'; break;
      case 0x58 : case 0x78 : return 'X'; break;
      case 0x59 : case 0x79 : return 'Y'; break;
      case 0x5A : case 0x7A : return 'Z'; break;
    }
    
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    return '?';
  }
  
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  short C2W_PostErrorMsg(string lt_ErrMsg)
  {
    // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    print(lt_ErrMsg);
    return -1;
  }
}









